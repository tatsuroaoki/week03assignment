// #2: GET THE PHONE NUMBER
const phoneNumber1 = '(206) 333-4444';
const phoneNumber2 = '206-333-4444';
const phoneNumber3 = '206 333 4444';
const phoneNumber4 = '206 333-4444';
const phoneNumber5 = '(206)-333 4444';

function testPhoneNumber(phoneNumber) {
	let regex1 = /(\()(\d{3})(\))( )(\d{3})(-)(\d{4})/;
	let regex2 = /(\d{3})(-)(\d{3})(-)(\d{4})/;
	let regex3 = /(\d{3})(\s)(\d{3})(\s)(\d{4})/;
	let isExisting;

	if(regex1.test(phoneNumber) || regex2.test(phoneNumber) || regex3.test(phoneNumber)) {
		isExisting = true;
	} else {
		isExisting = false;
	}
	return isExisting;
}

function parsePhoneNumber(phoneNumber) {
	let regex1 = /(\()(\d{3})(\))( )(\d{3})(-)(\d{4})/;
	let regex2 = /(\d{3})(-)(\d{3})(-)(\d{4})/;
	let regex3 = /(\d{3})(\s)(\d{3})(\s)(\d{4})/;
	let match;
	let phone = {
		areaCode:[],
		phoneNumber: []
	}

	if(regex1.test(phoneNumber)) {
		match = regex1.exec(phoneNumber);
		phone.areaCode = match[2];
		phone.phoneNumber = `${match[5]}${match[7]}`;
	} else if (regex2.test(phoneNumber)) {
		match = regex2.exec(phoneNumber);
		phone.areaCode = match[1];
		phone.phoneNumber = `${match[3]}${match[5]}`;
	} else if (regex3.exec(phoneNumber)) {
		match = regex3.exec(phoneNumber);
		phone.areaCode = match[1];
		phone.phoneNumber = `${match[3]}${match[5]}`;
	}
	return phone;
}

console.log(testPhoneNumber(phoneNumber3));
console.log(parsePhoneNumber(phoneNumber3));

// SOCCER STANDINGS

const RESULT_VALUES = {
	w: 3,
	d: 1,
	l: 0
}

// This function accepts one argument, the result, which should be a string
// Acceptable values are 'w', 'l', or 'd'
const getPointsFromResult = function getPointsFromResult(result) {
	return RESULT_VALUES[result];
}

const getTotalPoints = function getTotalPoints(results) {
	let totalPoints = 0;
	const games = results.length - 1;
	for (let i = 0; i <= games; i++) {
		totalPoints += getPointsFromResult(results.charAt(i));
	}
	return totalPoints;
}

const orderTeams = function () {
	const args = Array.from(arguments);
	args.forEach(function(team) {
		const totalPoints = getTotalPoints(team.results);
		console.log(`${team.name}: ${totalPoints}`);
	})
}

orderTeams(
	{name: 'Sounders', results: 'wwdl'},
	{name: 'Galaxy', results: 'wlld'},
);